json.array!(@comentarios) do |comentario|
  json.extract! comentario, :id, :nombre, :email, :texto
  json.url comentario_url(comentario, format: :json)
end
