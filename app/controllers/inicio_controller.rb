class InicioController < ApplicationController
  def index
    @comentario = Comentario.new
    @comentarios = Comentario.all.order('created_at DESC').take(5)
    @count_comentarios = Comentario.all.count
  end
end
