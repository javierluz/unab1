class CreateComentarios < ActiveRecord::Migration
  def change
    create_table :comentarios do |t|
      t.string :nombre
      t.string :email
      t.string :texto

      t.timestamps null: false
    end
  end
end
